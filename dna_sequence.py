dna = 'AABBBGGGKKDDDD'

def get_sequence(dna):
    sequence = ''
    previous_c = ''
    count = 0
    for c in dna:
        if c == previous_c:
            count += 1
        else:
            if len(previous_c) > 0:
                sequence += '{}{}'.format(count, previous_c)
            count = 1
            previous_c = c
    if count > 0:
        sequence += '{}{}'.format(count, previous_c)
    return sequence

print(get_sequence(dna))