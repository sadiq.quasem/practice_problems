import pandas as pd

"""
Given an array of arrays with cities, categories and count, restructure the
array to group the cities and convert the categories into columns with count
"""

array = [
["Boston","Mexican", "163"],
["Boston", "Seafood", "194"],
["Los Angeles","American", "1239"],
["Los Angeles","Mexican", "1389"],
["Los Angeles", "Seafood", "456"]
]

def solution(arr):
    # construct dataframe
    df = pd.DataFrame(array, columns=['city', 'category', 'value'])

    # convert to categories
    for col in ['city', 'category']:
        df[col] = df[col].astype('category')

    # groupby.first or groupby.sum works if you have unique combinations
    res = df.sort_values(['city', 'category'])\
            .groupby(['city', 'category']).first().fillna(0).reset_index()

    res_lst = res.groupby('city')['value'].apply(list).tolist()
    res_lst = [list(map(int, x)) for x in res_lst]

    print(res_lst)

solution(array)
