def findLongestSequence(mat):
    R = len(mat)
    C = len(mat[0])

    x = [0, 1, 0, -1]
    y = [1, 0, -1, 0]

    dp = [[-1 for i in range(C)] for i in range(R)]

    def isvalid( i, j):
        return (0 <= i < R) and (0 <= j < C)

    def getLenUtil(mat, i, j, expected):
        if not isvalid(i, j) or mat[i][j] != expected:
            return 0

        if dp[i][j] == -1:
            ans = 0
            expected = chr(ord(mat[i][j])+1)
            for k in range(4):
                ans = max(ans, 1 + getLenUtil(mat, i + x[k], j + y[k], expected))

            dp[i][j] = ans
        return dp[i][j]

    ans = 0
    for i in range(R):
        for j in range(C):
            getLenUtil(mat, i, j, mat[i][j])
        ans = max(ans, max(dp[i]))
    print(dp)
    return ans

res = findLongestSequence([["a","c","d"],["i","b","e"],["h","g","f"],["z","x","y"]])
print(res)
