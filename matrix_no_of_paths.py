"""
 [2, 0, 0 , 0]
 [0, 1, 0, 0]
 [0, 0, 1, 10]
 [0, 0, 10, 2]

[3,3]=[3,2]+[2,3]

Goal: Calculate number of paths you can reach to [2,3] from [0,0]:

[0,0]->[0,1]->[0,2]->[0,3]->[1,3]->[2,3]
"""

"""
Solution for path without obstacles
"""
def numberofpaths(n, m):
    if (m == 1 or n == 1):
        return 1
    return numberofpaths(n-1, m) + numberofpaths(n, m-1)
print(numberofpaths(n, m))

"""
Dynamic programming method
"""
def numberofpaths(n, m):
    dp = [[1 for _ in range(m)] for _ in range(n)]
    for i in range(n):
        for j in range(m):
            if i == 0 and j == 0:
                dp[i][j] = dp[i][j]
            elif j == 0:
                dp[i][j] = dp[i-1][j]
            elif i == 0:
                dp[i][j] = dp[i][j-1]
            else:
                dp[i][j] = dp[i][j-1]+dp[i-1][j]
    return dp[-1][-1]

"""
Dynamic programming method with obstacles
"""
def pathwithproblems(A):
    paths = [[0]*len(A[0] for i in A]
    if A[0][0] == 0:
        paths[0][0]=1
    for i in range(1, len(A)):
        if A[i][0] == 0:
            paths[i][0]=paths[i-1][0]
    for j in range(1, len(A)):
        if A[0][j] == 0:
            paths[0][j]=paths[0][j-1]
    for i in range(1, len(A)):
        for j in range(1, len(A)):
            if A[i][j] == 0:
                paths[i][j]=paths[i-1][j] + paths[i][j-1]
    return paths[-1][-1]
