"""
Find the most popular option with no duplicate votes and time constraint
"""

l = [['vanilla', '123', 7], ['vanilla','234', 8], ['vanilla','345', 9], ['vanilla','865', 8], ['chocolate','888', 5]]

d = {}

for i in l:
    userid = i[1]
    time = i[2]
    while time < 8 and any(userid.count(x) == 1 for x in userid):
        for j in i:
            if d.get(j):
                d[j] += 1
            else:
                d[j] = 1
        break
print(max(d, key=d.get))
